# Alarm-handler
Elettra alarm-handler Tango device


## Building and Installation

In its simplest form, clone the repository and assuming a standard install for all dependencies:

```
mkdir build
cd build
cmake ../
make
make install
```

### pkg-config Settings

The build system uses pkg-config to find some dependencies, for example Tango. If Tango is not installed to a standard location, set PKG_CONFIG_PATH, i.e.

```bash
export PKG_CONFIG_PATH=/non/standard/tango/install/location
...
cmake ../
...
```

The pkg-config path can also be set with the cmake argument CMAKE_PREFIX_PATH. This can be set on the command line at configuration time, i.e.:

```bash
...
cmake -DCMAKE_PREFIX_PATH=/non/standard/tango/install/location ../
...
```

### Project Flags

| Flag | Setting | Default | Description |
|------|-----|-----|-----|
| BUILD_TESTS | ON/OFF | OFF | Build test devices under test directory |

### Standard CMake Flags

The build system is CMake therefore standard CMake flags can be used to influence the build and installation process. The following is a list of common useful CMake flags and their use:

| Flag | Use |
|------|-----|
| CMAKE_INSTALL_PREFIX | Standard CMake flag to modify the install prefix. |
| CMAKE_INCLUDE_PATH | Standard CMake flag to add include paths to the search path. |
| CMAKE_LIBRARY_PATH | Standard CMake flag to add paths to the library search path |


## Legacy Building

Using Elettra makefiles, clone both alarm-handler and makefile repositories, then call make:

```bash
git clone http://gitlab.elettra.eu/cd/ds/makefiles.git
git clone http://gitlab.elettra.eu/cs/ds/alarm-handler.git
cd alarm-handler
make
```
