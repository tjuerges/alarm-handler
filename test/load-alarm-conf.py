#!/usr/bin/python
#

import sys,re
#import PyTango
from PyTango import *
import time
import string
	
if __name__ == "__main__":

	Device = False
	Conf = False
	File = False
	#PrepareState = False
	for arg in sys.argv:
		word = '([a-z0-9._\-\*]*)'
		wordpath = '([a-z0-9._\-\*/]*)'
		m = re.compile("--device=" + word + "/{0,1}" + word + "/{0,1}" + word).match(arg.lower())
		if m is not None:
			domain = m.groups()[0]
			family = m.groups()[1]
			member = m.groups()[2]
			if domain == '':
				domain = '*'
			if family == '':
				family = '*'
			if member == '':
				member = '*'
			Device = True
		#formula = '([a-z0-9._,\*,\-,\|,\/,\",\s,\t]*)'
		formula = '(.*)'
		m = re.compile("--conf=" + formula).match(arg)
		if m is not None:	
			alarm_rule = m.groups()[0]
			Conf = True
		#m = re.compile("--prepare_state").match(arg.lower())
		#if m is not None:
		#	PrepareState = True			
		m = re.compile("--file=" + wordpath).match(arg)
		if m is not None:
			file_name = m.groups()[0]
			File = True
	        	
		if Device:
			if Conf or File:
				dev_name = domain + '/' + family + '/' + member
				try:
					dev = DeviceProxy(dev_name)
				except (DevFailed,ConnectionFailed,EventSystemFailed) as e:
					print ('ERROR connecting proxy(',dev_name,'): ',e[0].desc)
					sys.exit(-1)				
			if Conf:					
				print ('Loading: ', alarm_rule)				
				try:
					dev.command_inout('Load',alarm_rule)
					print ('.............OK!')
				except (DevFailed,ConnectionFailed,EventSystemFailed) as e:
					print ('...........ERROR!')
					print ('     ---> ERROR: ', e[0].desc)
					continue						
				sys.exit(0)	
			elif File:
				for line in open(file_name):
					line = line[0:-1]
					try:					
						dev.command_inout('Load',line)
						time.sleep(0.1)						
					except (DevFailed,ConnectionFailed,EventSystemFailed) as e:
						print ('     ---> ERROR: ', e[0].desc)
						time.sleep(2)						
						sys.exit(1)					
				sys.exit(0)	
	#dev = DeviceProxy("alarm/alarm/1")
	#dev.command_inout('Load', sys.argv[1])

	if len(sys.argv) < 3:
		print ('Usage:', sys.argv[0], ' --device=alarm_device --conf=alarm_rule | --file=filename')
		print ()
		print ('Examples:')
		print ('\tload one alarm_rule in alarm/handler/01:', sys.argv[0], '--device=alarm/handler/01 --conf=\"tag=test0;formula=(alarm/test/01/condition == 1);on_delay=0;off_delay=0;priority=high;shlvd_time=0;group=gr_test;message=Test alarm;url=;on_command=;off_command=;enabled=1\"')
		print ('\tload alarm rules from filename in alarm/handler/01:', sys.argv[0], '--device=alarm/handler/01 --file=alarms.txt')
		sys.exit(-1)

# EOF
