# Project Name

testdevice

## Description

Tango device server for storing and distributing a number of variables as device attributes.

## Installation

See your institue guidelines for deploying and configuring a Tango device server.

## Usage

The server is dynamically configured by means of dedicated Methods. The configuration and values are stored in the Tango Database and restored at server restart.
A typical usage is to store and distribute the results of measurments or simulations to a number of differet clients.

## History


## Credits


Elettra-Sincrotrone Trieste S.C.p.A. di interesse nazionale
Strada Statale 14 - km 163,5 in AREA Science Park
34149 Basovizza, Trieste ITALY

## License

GPL 3
