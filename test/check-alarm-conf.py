#!/usr/bin/python
#

import sys,re
#import PyTango
from PyTango import *
import time
import string
	
if __name__ == "__main__":

	Device = False
	Conf = False
	File = False
	conflist = []
	#PrepareState = False
	for arg in sys.argv:
		word = '([a-z0-9._\-\*]*)'
		wordpath = '([a-z0-9._\-\*/]*)'
		m = re.compile("--device=" + word + "/{0,1}" + word + "/{0,1}" + word).match(arg.lower())
		if m is not None:
			#print m.groups()
			domain = m.groups()[0]
			family = m.groups()[1]
			member = m.groups()[2]
			if domain == '':
				domain = '*'
			if family == '':
				family = '*'
			if member == '':
				member = '*'
			Device = True
		#formula = '([a-z0-9._,\*,\-,\|,\/,\",\s,\t]*)'
		formula = '(.*)'
		m = re.compile("--conf=" + formula).match(arg)
		if m is not None:
			#print m.groups()	
			alarm_rule = m.groups()[0]
			Conf = True
		#m = re.compile("--prepare_state").match(arg.lower())
		#if m is not None:
		#	PrepareState = True			
		m = re.compile("--file=" + wordpath).match(arg)
		if m is not None:
			#print m.groups()
			file_name = m.groups()[0]
			File = True
	        
		tagchars = '([a-zA-Z0-9._-]*)'
		tag=''
		if Device:
			if Conf or File:
				dev_name = domain + '/' + family + '/' + member
				try:
					dev = DeviceProxy(dev_name)
				except (DevFailed,ConnectionFailed,EventSystemFailed) as e:
					print ('ERROR connecting proxy(',dev_name,'): ',e[0].desc)
					sys.exit(-1)				
			if Conf:									
				m = re.compile("tag=" + tagchars + ";(.*)").match(alarm_rule)
				if m is not None:
					try:
						tag = m.groups()[0]
						conflist = dev.command_inout('SearchAlarm',tag)
					except (DevFailed,ConnectionFailed,EventSystemFailed) as e:
						print ('     ---> ERROR: ', e[0].desc)
						sys.exit(1)
					for co in conflist:
						if co == alarm_rule:
							print ('Found matching alarm: ', co)
							sys.exit(0)
				print ('Not found conf for alarm ', tag)			
				sys.exit(1)
			elif File:
				for line in open(file_name):
					line = line[0:-1]
					m = re.compile("tag=" + tagchars + ";(.*)").match(line)
					conf_found = False
					if m is not None:
						try:
							tag = m.groups()[0]
							conflist = dev.command_inout('SearchAlarm',tag)
						except (DevFailed,ConnectionFailed,EventSystemFailed) as e:
							print ('     ---> ERROR: ', e[0].desc)
							sys.exit(1)
						for co in conflist:
							if co == alarm_rule:
								conf_found = True
						if not conf_found:
							print ('Not found conf for alarm ', tag)				
							sys.exit(1)

				sys.exit(0)

	if len(sys.argv) < 3:
		print ('Usage:', sys.argv[0], ' --device=alarm_device --conf=alarm_rule | --file=filename')
		print ()
		print ('Examples:')
		print ('\tcheck one alarm_rule in alarm/handler/01:', sys.argv[0], '--device=alarm/handler/01 --conf=\"tag=test0;formula=(alarm/test/01/condition == 1);on_delay=0;off_delay=0;priority=high;shlvd_time=0;group=gr_test;message=Test alarm;url=;on_command=;off_command=;enabled=1\"')
		print ('\tcheck alarm rules from filename in alarm/handler/01:', sys.argv[0], '--device=alarm/handler/01 --file=alarms.txt')
		sys.exit(-1)

# EOF
