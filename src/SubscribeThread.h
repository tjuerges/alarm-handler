#ifndef _SUBSCRIBE_THREAD_H
#define _SUBSCRIBE_THREAD_H

#include <tango.h>
#include <eventconsumer.h>
#include <stdint.h>
#include "event_table.h"

//	constants definitions here.
//-----------------------------------------------

namespace AlarmHandler_ns
{

//class ArchiveCB;
class AlarmHandler;

class SubscribeThread;

//=========================================================
/**
 *	Create a thread retry to subscribe event.
 */
//=========================================================
class SubscribeThread: public omni_thread, public Tango::LogAdapter
{
private:
	/**
	 *	Shared data
	 */
	event_table	*shared;
	/**
	 *	HdbDevice object
	 */
	AlarmHandler	*alarm_dev;


public:
	int			period;
	SubscribeThread(AlarmHandler *dev);
	void updateProperty();
	void signal();
	/**
	 *	Execute the thread loop.
	 *	This thread is awaken when a command has been received 
	 *	and falled asleep when no command has been received from a long time.
	 */
	void *run_undetached(void *);
	void start() {start_undetached();}
};


}	// namespace_ns

#endif	// _SUBSCRIBE_THREAD_H
