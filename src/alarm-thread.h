/*
 * alarm-thread.h
 *
 * copyleft: Sincrotrone Trieste S.C.p.A. di interesse nazionale
 *           Strada Statale 14 - km 163,5 in AREA Science Park
 *           34012 Basovizza, Trieste ITALY
 */

#ifndef ALARM_THREAD_H
#define ALARM_THREAD_H

#include <omnithread.h>
#include <tango.h>
#include <AlarmHandler.h>

#define ALARM_THREAD_EXIT				"alarm_thread_exit"
#define ALARM_THREAD_EXIT_VALUE	-100
#define ALARM_THREAD_TO_BE_EVAL			"to_be_evaluated"
#define ALARM_THREAD_TO_BE_EVAL_VALUE	-200

class alarm_thread : public omni_thread {
	public:
		alarm_thread(AlarmHandler_ns::AlarmHandler *p);
		~alarm_thread();
		//int period;
	protected:
		void run(void *);
	private:
		AlarmHandler_ns::AlarmHandler *p_Alarm;
};

#endif	/* ALARM_THREAD_H */

/* EOF */
