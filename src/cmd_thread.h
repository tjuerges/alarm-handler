/*
 * cmd_thread.h
 *
 * copyleft: Sincrotrone Trieste S.C.p.A. di interesse nazionale
 *           Strada Statale 14 - km 163,5 in AREA Science Park
 *           34012 Basovizza, Trieste ITALY
 */

#ifndef CMD_THREAD_H
#define CMD_THREAD_H

#ifndef _LOGA
#define _LOGA	1
#endif		// _LOGA

//#define CMD_THREAD_EXIT	"EXIT"
#define CMD_COMMAND		1
#define CMD_RESPONSE	2
#define CMD_THREAD_EXIT	3

#include <omnithread.h>
#include <tango.h>
#include "AlarmHandler.h"

struct cmd_t
{
	short cmd_id;
	long call_id;
	string arg_s1;
	string arg_s2;
	string arg_s3;
	bool arg_b;
	long dp_add;
};

class cmd_list : public omni_mutex {
	public:
		cmd_list(void): empty(this) {}
		~cmd_list(void) {}
		void push_back(cmd_t& cmd);
		const cmd_t pop_front(void);
		void clear(void);
	protected:
		list<cmd_t> l_cmd;
	private:
		omni_condition empty;
};

class cmd_thread : public omni_thread{
	public:
		cmd_thread();
		~cmd_thread();
		cmd_list list;
		//omni_mutex::omni_mutex *mutex_dp;
	protected:
		void run(void *);
	private:
		Tango::DeviceProxy *dp;
};

#endif	/* CMD_THREAD_H */

/* EOF */
