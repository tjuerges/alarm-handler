/*
 * update-thread.cpp
 */

#include "update-thread.h"
static const char __FILE__rev[] = __FILE__ " $Revision: 1.2 $";

/*
 * alarm_thread::alarm_thread()
 */
update_thread::update_thread(AlarmHandler_ns::AlarmHandler *p) : p_Alarm(p),Tango::LogAdapter(p)
{
	//cout << __FILE__rev << endl;
}

/*
 * alarm_thread::~alarm_thread()
 */
update_thread::~update_thread()
{
	DEBUG_STREAM << __func__ << "update_thread::~update_thread(): entering!" << endl;
	p_Alarm = NULL;
}

/*
 * alarm_thread::run()
 */
void update_thread::run(void *)
{
	DEBUG_STREAM << __func__ << "update_thread::run(): entering!" << endl;
	//printf("update_thread::run(): running...\n");
	unsigned int pausasec, pausanano;
	pausasec=1;
	pausanano = 100000000;		//0.1 sec
	omni_thread::sleep(pausasec,pausanano);
	while (!p_Alarm->abortflag) {
		try
		{
			//omni_thread::sleep(pausasec,pausanano);
			abort_sleep(pausasec+(double)pausanano/1000000000);
			if(!p_Alarm->abortflag)
				p_Alarm->timer_update();
			//printf("update_thread::run(): TIMER!!\n");
		}		
		catch(...)
		{
			INFO_STREAM << "update_thread::run(): catched unknown exception!!";
		}		
	}
	DEBUG_STREAM << "update_thread::run(): exiting!" << endl;
}  /* update_thread::run() */

void update_thread::abort_sleep(double time)
{
	omni_mutex_lock sync(*this);
	long time_millis = (long)(time*1000);
	int res = wait(time_millis);
}
