/*
 * update-thread.h
 */

#ifndef UPDATE_THREAD_H
#define UPDATE_THREAD_H

#include <omnithread.h>
#include <tango.h>
#include <AlarmHandler.h>

class update_thread : public omni_thread, public Tango::TangoMonitor, public Tango::LogAdapter {
	public:
		update_thread(AlarmHandler_ns::AlarmHandler *p);
		~update_thread();
	protected:
		void run(void *);
	private:
		void abort_sleep(double time);
		AlarmHandler_ns::AlarmHandler *p_Alarm;
};

#endif	/* UPDATE_THREAD_H */

/* EOF */
